import { type NextPageWithLayout } from "./_app";

import ThemeLayout from "@/layout/ThemeLayout";

const Home: NextPageWithLayout = () => {
  return <p className="text-pink-500 dark:text-yellow-500 font-sans">Hello</p>;
};

Home.getLayout = (page) => <ThemeLayout>{page}</ThemeLayout>;

export default Home;
